<?php

/**
 * @file
 * Menu items for uc_wepay module
 * 
 * Instant payment notification callback and settings forms
 * 
 */

/**
 * IPN callback page reads the checkout_id from WePay callback, sends API call
 * to get checkout details and updates order.
 */
function uc_wepay_ipn() {
  if (isset($_POST['checkout_id'])) {

    // Check the status of checkout.
    $params = array(
      'checkout_id' => $_POST['checkout_id'],
    );
    $checkout = uc_wepay_do('/checkout', $params);

    // Make sure the WePay API did not return error.
    if (is_array($checkout)) {
      if (isset($checkout['error'])) {
        watchdog('uc_wepay', 'Error type @error occured. Description: @description. Checkout ID: @id', array('@error' => $checkout['error'], '@description' => $checkout['error_description'], '@id' => $_POST['checkout_id']), WATCHDOG_ERROR);
        return;
      };
    }

    // Load appropriate order.
    $order_id = db_result(db_query('SELECT order_id FROM {uc_wepay_checkout} WHERE checkout_id = %d', $_POST['checkout_id']));
    if (!is_numeric($order_id) || $order_id == 0) {
      watchdog('uc_wepay', 'Could not find order related to the checkout ID. Checkout ID: @id', array('@id' => $_POST['checkout_id']), WATCHDOG_ERROR);
      return;
    }
    $order = uc_order_load($order_id);

    // Make sure the order is valid.
    if (!$order) {
      watchdog('uc_wepay', 'Could not load order related to the checkout ID. Checkout ID: @id', array('@id' => $_POST['checkout_id']), WATCHDOG_ERROR);
      return;
    }

    if ($checkout->state == 'captured') {
      $comment = t('WePay checkout id: @cid', array('@cid' => $_POST['checkout_id']));
      uc_payment_enter($order_id, 'wepay', $order->order_total, $order->uid, NULL, $comment);
      uc_cart_complete_sale($order);
      uc_order_update_status($order_id, 'payment_received');
      uc_order_comment_save($order_id, 0, t('Payment captured through WePay.'), 'order', 'payment_received');
    }
  }
  return 'IPN placeholder page';
}

/**
 * Form for selection of default payment account from the list of available ones
 */
function uc_wepay_admin_settings() {

  // Make the API call -- list account on selected server.
  $accounts = uc_wepay_do('/account/find');

  // Escape the form in case of error.
  if (isset($accounts['error'])) {
    drupal_set_message(check_plain($accounts['error_description']), 'error');
    return;
  }

  if (isset($accounts[0]->account_id)) {
    // If there are accounts on the given server, offer the system for to choose for account.
    $form['uc_wepay_account_id'] = array(
      '#type' => 'radios',
      '#title' => t('Default payment account'),
      '#description' => t('Please choose default payment account. NB: every change of WePay server (from staging to production and vice versa) will clear the selection.'),
      '#default_value' => variable_get('uc_wepay_account_id', ''),
      '#options' => array(),
    );
    foreach ($accounts as $account) {
      $form['uc_wepay_account_id']['#options'][$account->account_id] = $account->name;
    }
  }
  else {
    // No accounts were created -- offer the possiblity to create one.
    drupal_set_message(t('You still do not have accounts to hold the payments in WePay. Go to <a href="@new-account">new account page</a> to create one', array('@new-account' => url('admin/store/settings/uc_wepay/new'))));
    return;
  };

  return system_settings_form($form);
}

/**
 * Form for new account creation
 */
function uc_wepay_admin_new_account() {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account name'),
    '#description' => 'The name of the account you want to create.',
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Account description'),
    '#description' => 'The description of the account you want to create.',
    '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Create account'));
  $form['#redirect'] = 'admin/store/settings/uc_wepay/settings';

  return $form;
}

function uc_wepay_admin_new_account_submit($form, &$form_state) {
  $params = array(
    'name' => $form_state['values']['name'],
    'description' => $form_state['values']['description'],
  );
  $result = uc_wepay_do('/account/create', $params);

  // If the account creation failed, provide appropriate error message.
  if (is_array($result) && isset($result['error'])) {
    drupal_set_message('<strong>' . check_plain($result['error']) . '</strong> ' . check_plain($result['error_description']), 'error');
  }
  else {
    drupal_set_message('Account successfully created');
  }
}