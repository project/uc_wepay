KNOWN PROBLEMS
--------------

  WePay API insists on providing checkout type when defining a new checkout.
  Allowed values are: GOODS, SERVICE, DONATION, or PERSONAL. Problem is when one
  cart contains multiple product types: SERVICE and GOODS, for example. For now,
  logic behind module is simple but wrong -- if any of items in cart is
  shippable, it's GOODS checkout, otherwise it's SERVICE. Module still does not
  implement DONATION or PERSONAL checkout types.


CREDITS
-------

  Developed and maintained by:
    Branislav Bujisic <http://drupal.org/user/52799>
  
  Sponsored by:
    Grrrinders Club LLC <http://www.grrrinders.com/>